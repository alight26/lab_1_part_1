/*
Alexandra Light
EGR226  Lab1
Zuidema 8/29/2019

calls equations
****************/

#include "stats_lib.h"
#include <stdio.h>
#include <stdlib.h>
#include "stats_lib.c"


int main()
{                                                   //write the code necessary to read in an array of
    FILE *myFile;
    myFile = fopen("data.txt", "r");
   // printf("File opened\n\n");                                                //numbers from a file called “data.txt”
     float nums[1000];
     int n=0;

        while(!feof(myFile))
        {
            //printf("WHILE LOOP\n");
            fscanf(myFile, "%f\n", &nums[n]);
            //printf("%f \n", nums[n]);
            n++;
        }

        //printf("Equations\n\n");
        float First, Second, Third, Fourth, Fifth, Sixth;       //setting float variable

        First = maximum(nums, n);
        Second = minimum(nums,n);
        Third = mean(nums,n);                                   //equation returns being stored
        Fourth = median(nums,n);
        Fifth = variance(nums,n);
        Sixth = standard_deviation(nums,n);

        printf("Max: %f\nMin: %f\nMean: %f\nMedian: %f\nVariance: %f\nStandard Deviation: %f\n\n",First, Second, Third, Fourth, Fifth, Sixth);

    return 0;
}