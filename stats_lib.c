/*
Alexandra Light
EGR226  Lab1
Zuidema

Equations
*/

float maximum(float nums[ ], int n);
float minimum (float nums[ ], int n);
float mean(float nums[ ], int n);
float median(float nums[ ], int n);
float variance(float nums[ ], int n);
float standard_deviation(float nums[ ], int n);
/*
Alexandra Light
EGR226  Lab1
Zuidema

.c file, the beef of the equations
*/

#include "stats_lib.h"
#include <math.h>

int MinVal, MaxVal, max, min, i;

float maximum(float nums[ ], int n)
{
    max = 0;
    for (i = 1; i < n; i++)
    {                           //	This function should loop through the provided n-element array of
        if (nums[i] > max)
        {                       //        numbers and return the largest number in the array.
        MaxVal = i;
        max = nums[i];
        }
  }
  return max;
}

float minimum(float nums[ ], int n)
{                                           //	This function should loop through the provided n-element array of
                                            //       numbers and return the smallest number in the array.
    for (i = 1; i < n; i++)
    {
        if (nums[i] < min)
        {
        MinVal = i;
        min = nums[i];
        }
  }
  return min;
    }
float mean(float nums[ ], int n)
{     float total=0;
      float Average=0 ;
                                       //	This function should calculate and return the mean
      for (i = 0; i < n; i++)
        {
            total = total + nums[i] ;
        }
          Average = total/n;
          return Average;
}

float median(float nums[ ], int n)
{ float TotalMedian;
    int i, j, temp;                     //	This function should sort the provided n-element array of numbers
       for(i=0; i<n-1; i++)             //      and return the number at the n/2 index of the sorted array
        {
        for(j=i+1; j<n; j++)
        {
            if(nums[j] < nums[i])
                {
                temp = nums[i];
                nums[i] = nums[j];
                nums[j] = temp;
                }
        }
        }
        if(n%2==0)
        {
          TotalMedian = (nums[n/2] + nums[n/2 - 1] )/ 2.0;
          return TotalMedian;
        }
        else
        {
            TotalMedian = nums[n/2];
            return TotalMedian;
        }
       return TotalMedian;
}
float variance(float nums[ ], int n)
{
    float Average=0;
    Average = mean(nums,n);
    float var=0, diff=0, square=0, Variance=0;                //	This function should calculate and return the variance of
    for (i = 0; i < n; i++)                 //      the data set using the following equation,
    {
       diff = nums[i] - Average;
       square = diff * diff;
       var = square + var;                  //DOES THIS WORK HOW I WANT IT TO
    }
    Variance = var / (n-1);
    return Variance;
}
float standard_deviation(float nums[], int n)
{     float var, stdev;                                                    //This function should calculate and return the standard
      var = variance(nums,n);                //  deviation of the data set.  Standard deviation is found by taking
      stdev = sqrt(var);
      return stdev;                      //  the square root of a data set’s variance. HINT: call your variance(…)
}                                           //  function inside your standard_deviation(…) function

